// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "TrackRacingGBWheelRear.generated.h"

UCLASS()
class UTrackRacingGBWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UTrackRacingGBWheelRear();
};



