// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "TrackRacingGBHud.generated.h"


UCLASS(config = Game)
class ATrackRacingGBHud : public AHUD
{
	GENERATED_BODY()

public:
	ATrackRacingGBHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface
};
