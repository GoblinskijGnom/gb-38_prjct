// Copyright Epic Games, Inc. All Rights Reserved.

#include "TrackRacingGBWheelFront.h"

UTrackRacingGBWheelFront::UTrackRacingGBWheelFront()
{
	ShapeRadius = 35.f;
	ShapeWidth = 10.0f;
	bAffectedByHandbrake = false;
	SteerAngle = 50.f;
}
