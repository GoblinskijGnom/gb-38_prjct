// Copyright Epic Games, Inc. All Rights Reserved.

#include "TrackRacingGBWheelRear.h"

UTrackRacingGBWheelRear::UTrackRacingGBWheelRear()
{
	ShapeRadius = 35.f;
	ShapeWidth = 10.0f;
	bAffectedByHandbrake = true;
	SteerAngle = 0.f;
}
