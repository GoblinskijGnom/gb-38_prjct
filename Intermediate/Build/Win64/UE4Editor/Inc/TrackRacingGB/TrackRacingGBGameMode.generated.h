// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TRACKRACINGGB_TrackRacingGBGameMode_generated_h
#error "TrackRacingGBGameMode.generated.h already included, missing '#pragma once' in TrackRacingGBGameMode.h"
#endif
#define TRACKRACINGGB_TrackRacingGBGameMode_generated_h

#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_SPARSE_DATA
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_RPC_WRAPPERS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_RPC_WRAPPERS_NO_PURE_DECLS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATrackRacingGBGameMode(); \
	friend struct Z_Construct_UClass_ATrackRacingGBGameMode_Statics; \
public: \
	DECLARE_CLASS(ATrackRacingGBGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), TRACKRACINGGB_API) \
	DECLARE_SERIALIZER(ATrackRacingGBGameMode)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_INCLASS \
private: \
	static void StaticRegisterNativesATrackRacingGBGameMode(); \
	friend struct Z_Construct_UClass_ATrackRacingGBGameMode_Statics; \
public: \
	DECLARE_CLASS(ATrackRacingGBGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), TRACKRACINGGB_API) \
	DECLARE_SERIALIZER(ATrackRacingGBGameMode)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TRACKRACINGGB_API ATrackRacingGBGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATrackRacingGBGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TRACKRACINGGB_API, ATrackRacingGBGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATrackRacingGBGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TRACKRACINGGB_API ATrackRacingGBGameMode(ATrackRacingGBGameMode&&); \
	TRACKRACINGGB_API ATrackRacingGBGameMode(const ATrackRacingGBGameMode&); \
public:


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TRACKRACINGGB_API ATrackRacingGBGameMode(ATrackRacingGBGameMode&&); \
	TRACKRACINGGB_API ATrackRacingGBGameMode(const ATrackRacingGBGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TRACKRACINGGB_API, ATrackRacingGBGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATrackRacingGBGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATrackRacingGBGameMode)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_PRIVATE_PROPERTY_OFFSET
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_6_PROLOG
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_RPC_WRAPPERS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_INCLASS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_INCLASS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h_9_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TRACKRACINGGB_API UClass* StaticClass<class ATrackRacingGBGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TrackRacingGB_Source_TrackRacingGB_TrackRacingGBGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
