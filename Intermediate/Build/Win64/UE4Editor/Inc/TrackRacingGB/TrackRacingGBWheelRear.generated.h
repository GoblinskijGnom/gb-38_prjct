// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TRACKRACINGGB_TrackRacingGBWheelRear_generated_h
#error "TrackRacingGBWheelRear.generated.h already included, missing '#pragma once' in TrackRacingGBWheelRear.h"
#endif
#define TRACKRACINGGB_TrackRacingGBWheelRear_generated_h

#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_SPARSE_DATA
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_RPC_WRAPPERS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTrackRacingGBWheelRear(); \
	friend struct Z_Construct_UClass_UTrackRacingGBWheelRear_Statics; \
public: \
	DECLARE_CLASS(UTrackRacingGBWheelRear, UVehicleWheel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), NO_API) \
	DECLARE_SERIALIZER(UTrackRacingGBWheelRear)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUTrackRacingGBWheelRear(); \
	friend struct Z_Construct_UClass_UTrackRacingGBWheelRear_Statics; \
public: \
	DECLARE_CLASS(UTrackRacingGBWheelRear, UVehicleWheel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), NO_API) \
	DECLARE_SERIALIZER(UTrackRacingGBWheelRear)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTrackRacingGBWheelRear(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTrackRacingGBWheelRear) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTrackRacingGBWheelRear); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTrackRacingGBWheelRear); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTrackRacingGBWheelRear(UTrackRacingGBWheelRear&&); \
	NO_API UTrackRacingGBWheelRear(const UTrackRacingGBWheelRear&); \
public:


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTrackRacingGBWheelRear(UTrackRacingGBWheelRear&&); \
	NO_API UTrackRacingGBWheelRear(const UTrackRacingGBWheelRear&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTrackRacingGBWheelRear); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTrackRacingGBWheelRear); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTrackRacingGBWheelRear)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_PRIVATE_PROPERTY_OFFSET
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_9_PROLOG
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_RPC_WRAPPERS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_INCLASS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_INCLASS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TRACKRACINGGB_API UClass* StaticClass<class UTrackRacingGBWheelRear>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TrackRacingGB_Source_TrackRacingGB_TrackRacingGBWheelRear_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
