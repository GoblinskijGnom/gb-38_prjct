// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TrackRacingGB/TrackRacingGBWheelFront.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTrackRacingGBWheelFront() {}
// Cross Module References
	TRACKRACINGGB_API UClass* Z_Construct_UClass_UTrackRacingGBWheelFront_NoRegister();
	TRACKRACINGGB_API UClass* Z_Construct_UClass_UTrackRacingGBWheelFront();
	PHYSXVEHICLES_API UClass* Z_Construct_UClass_UVehicleWheel();
	UPackage* Z_Construct_UPackage__Script_TrackRacingGB();
// End Cross Module References
	void UTrackRacingGBWheelFront::StaticRegisterNativesUTrackRacingGBWheelFront()
	{
	}
	UClass* Z_Construct_UClass_UTrackRacingGBWheelFront_NoRegister()
	{
		return UTrackRacingGBWheelFront::StaticClass();
	}
	struct Z_Construct_UClass_UTrackRacingGBWheelFront_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTrackRacingGBWheelFront_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UVehicleWheel,
		(UObject* (*)())Z_Construct_UPackage__Script_TrackRacingGB,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTrackRacingGBWheelFront_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TrackRacingGBWheelFront.h" },
		{ "ModuleRelativePath", "TrackRacingGBWheelFront.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTrackRacingGBWheelFront_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTrackRacingGBWheelFront>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTrackRacingGBWheelFront_Statics::ClassParams = {
		&UTrackRacingGBWheelFront::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTrackRacingGBWheelFront_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTrackRacingGBWheelFront_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTrackRacingGBWheelFront()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTrackRacingGBWheelFront_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTrackRacingGBWheelFront, 504202378);
	template<> TRACKRACINGGB_API UClass* StaticClass<UTrackRacingGBWheelFront>()
	{
		return UTrackRacingGBWheelFront::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTrackRacingGBWheelFront(Z_Construct_UClass_UTrackRacingGBWheelFront, &UTrackRacingGBWheelFront::StaticClass, TEXT("/Script/TrackRacingGB"), TEXT("UTrackRacingGBWheelFront"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTrackRacingGBWheelFront);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
