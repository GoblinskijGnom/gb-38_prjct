// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TRACKRACINGGB_TrackRacingGBPawn_generated_h
#error "TrackRacingGBPawn.generated.h already included, missing '#pragma once' in TrackRacingGBPawn.h"
#endif
#define TRACKRACINGGB_TrackRacingGBPawn_generated_h

#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_SPARSE_DATA
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_RPC_WRAPPERS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATrackRacingGBPawn(); \
	friend struct Z_Construct_UClass_ATrackRacingGBPawn_Statics; \
public: \
	DECLARE_CLASS(ATrackRacingGBPawn, AWheeledVehicle, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), NO_API) \
	DECLARE_SERIALIZER(ATrackRacingGBPawn)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_INCLASS \
private: \
	static void StaticRegisterNativesATrackRacingGBPawn(); \
	friend struct Z_Construct_UClass_ATrackRacingGBPawn_Statics; \
public: \
	DECLARE_CLASS(ATrackRacingGBPawn, AWheeledVehicle, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), NO_API) \
	DECLARE_SERIALIZER(ATrackRacingGBPawn)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATrackRacingGBPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATrackRacingGBPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATrackRacingGBPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATrackRacingGBPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATrackRacingGBPawn(ATrackRacingGBPawn&&); \
	NO_API ATrackRacingGBPawn(const ATrackRacingGBPawn&); \
public:


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATrackRacingGBPawn(ATrackRacingGBPawn&&); \
	NO_API ATrackRacingGBPawn(const ATrackRacingGBPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATrackRacingGBPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATrackRacingGBPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATrackRacingGBPawn)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(ATrackRacingGBPawn, SpringArm); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ATrackRacingGBPawn, Camera); } \
	FORCEINLINE static uint32 __PPO__InternalCameraBase() { return STRUCT_OFFSET(ATrackRacingGBPawn, InternalCameraBase); } \
	FORCEINLINE static uint32 __PPO__InternalCamera() { return STRUCT_OFFSET(ATrackRacingGBPawn, InternalCamera); } \
	FORCEINLINE static uint32 __PPO__InCarSpeed() { return STRUCT_OFFSET(ATrackRacingGBPawn, InCarSpeed); } \
	FORCEINLINE static uint32 __PPO__InCarGear() { return STRUCT_OFFSET(ATrackRacingGBPawn, InCarGear); }


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_14_PROLOG
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_RPC_WRAPPERS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_INCLASS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_INCLASS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TRACKRACINGGB_API UClass* StaticClass<class ATrackRacingGBPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TrackRacingGB_Source_TrackRacingGB_TrackRacingGBPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
