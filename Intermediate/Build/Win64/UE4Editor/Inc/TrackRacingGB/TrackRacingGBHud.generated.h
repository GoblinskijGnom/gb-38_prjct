// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TRACKRACINGGB_TrackRacingGBHud_generated_h
#error "TrackRacingGBHud.generated.h already included, missing '#pragma once' in TrackRacingGBHud.h"
#endif
#define TRACKRACINGGB_TrackRacingGBHud_generated_h

#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_SPARSE_DATA
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_RPC_WRAPPERS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATrackRacingGBHud(); \
	friend struct Z_Construct_UClass_ATrackRacingGBHud_Statics; \
public: \
	DECLARE_CLASS(ATrackRacingGBHud, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), NO_API) \
	DECLARE_SERIALIZER(ATrackRacingGBHud)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_INCLASS \
private: \
	static void StaticRegisterNativesATrackRacingGBHud(); \
	friend struct Z_Construct_UClass_ATrackRacingGBHud_Statics; \
public: \
	DECLARE_CLASS(ATrackRacingGBHud, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TrackRacingGB"), NO_API) \
	DECLARE_SERIALIZER(ATrackRacingGBHud)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATrackRacingGBHud(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATrackRacingGBHud) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATrackRacingGBHud); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATrackRacingGBHud); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATrackRacingGBHud(ATrackRacingGBHud&&); \
	NO_API ATrackRacingGBHud(const ATrackRacingGBHud&); \
public:


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATrackRacingGBHud(ATrackRacingGBHud&&); \
	NO_API ATrackRacingGBHud(const ATrackRacingGBHud&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATrackRacingGBHud); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATrackRacingGBHud); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATrackRacingGBHud)


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_PRIVATE_PROPERTY_OFFSET
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_7_PROLOG
#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_RPC_WRAPPERS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_INCLASS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_PRIVATE_PROPERTY_OFFSET \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_SPARSE_DATA \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_INCLASS_NO_PURE_DECLS \
	TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TRACKRACINGGB_API UClass* StaticClass<class ATrackRacingGBHud>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TrackRacingGB_Source_TrackRacingGB_TrackRacingGBHud_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
